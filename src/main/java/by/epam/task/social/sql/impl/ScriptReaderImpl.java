package by.epam.task.social.sql.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.task.social.sql.ScriptReader;

public class ScriptReaderImpl implements ScriptReader {
	private static final Logger LOGGER = Logger.getLogger(ScriptReaderImpl.class);
	private static final String[] COMMENT_SIGNS = { "#", "--", "/*", "*/" };
	private static final String DELIMETER = ";";
	private static final String PL_SQL_START = "declare";
	private boolean commentStarted = false;

	@Override
	public List<String> createQueries(String path) {
		String queryLine = null;
		StringBuilder stringBuilder = new StringBuilder();
		String sql = null;
		try (BufferedReader br = new BufferedReader(new FileReader(new File(path)))) {
			while ((queryLine = br.readLine()) != null) {
				if (commentStarted) {
					queryLine = null;
				}
				for (String commentSign : COMMENT_SIGNS) {
					removeComments(queryLine, commentSign);
				}
				if (queryLine != null && !queryLine.equals("")) {
					stringBuilder.append(queryLine.trim()).append(" ");
				}
			}
			sql = stringBuilder.toString().trim();
		} catch (Exception e) {
			LOGGER.error("Can't create queries from sql file", e);
		}
		return sql.startsWith(PL_SQL_START) ? Arrays.asList(new String[] { sql }) : Arrays.asList(sql.split(DELIMETER));
	}

	private void removeComments(String queryLine, String commentSign) {
		int indexOfCommentSign = queryLine.indexOf(commentSign);
		if (indexOfCommentSign != -1) {
			commentStarted = commentSign.equals(COMMENT_SIGNS[2]);
			if (commentStarted && commentSign.equals(COMMENT_SIGNS[3])) {
				commentStarted = !commentStarted;
			}
			queryLine = queryLine.startsWith(commentSign) ? queryLine = null
					: trimQueryString(queryLine, commentSign, indexOfCommentSign);
		}
	}

	private String trimQueryString(String queryLine, String commentSign, int indexOfCommentSign) {
		return commentSign.equals(COMMENT_SIGNS[3])
				? new String(queryLine.substring(indexOfCommentSign + 2, queryLine.length() - 1))
				: new String(queryLine.substring(0, indexOfCommentSign - 1));
	}
}
