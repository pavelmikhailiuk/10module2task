package by.epam.task.social.sql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.task.social.sql.DBConnector;
import by.epam.task.social.sql.SqlRunner;

public class SqlRunnerImpl implements SqlRunner {
	private static final Logger LOGGER = Logger.getLogger(SqlRunnerImpl.class);

	@Override
	public void run(List<String> sqlQueries) {
		try (Connection connection = DBConnector.getConnection()) {
			connection.setAutoCommit(false);
			for (String query : sqlQueries) {
				try {
					PreparedStatement statement = connection.prepareStatement(query);
					statement.execute();
					LOGGER.info("Query: " + query + " executed");
					ResultSet resultSet = statement.getResultSet();
					if (resultSet != null) {
						while (resultSet.next()) {
							int i = 1;
							LOGGER.info("Query result: " + resultSet.getString(i));
							i++;
						}
					}
				} catch (SQLException e) {
					LOGGER.error("Can't run sql script line: " + query, e);
					connection.rollback();
					LOGGER.info("Rollback all data");
					throw new RuntimeException("Script run is stopped");
				}
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (Exception e) {
			LOGGER.error("Can't run sql script", e);
		}
	}
}
