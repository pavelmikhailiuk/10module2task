package by.epam.task.social.sql;

import java.util.List;

public interface ScriptReader {
	List<String> createQueries(String path);
}
