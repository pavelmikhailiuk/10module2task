package by.epam.task.social.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public class DBConnector {
	private static ResourceBundle dbConfig = ResourceBundle.getBundle("orcl");
	private static final Logger LOGGER = Logger.getLogger(DBConnector.class);

	public static Connection getConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(dbConfig.getString("url"), dbConfig.getString("username"),
					dbConfig.getString("password"));
		} catch (SQLException e) {
			LOGGER.error(e);
			throw new RuntimeException("Can't connect to database");
		}
		return connection;
	}
}