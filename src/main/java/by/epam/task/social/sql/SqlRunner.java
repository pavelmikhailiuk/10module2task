package by.epam.task.social.sql;

import java.util.List;

public interface SqlRunner {
	void run(List<String> sqlQueries);
}
