package by.epam.task.social.main;

import by.epam.task.social.sql.ScriptReader;
import by.epam.task.social.sql.SqlRunner;
import by.epam.task.social.sql.impl.ScriptReaderImpl;
import by.epam.task.social.sql.impl.SqlRunnerImpl;

public class Runner {
	public static void main(String[] args) {
		ScriptReader scriptReader = new ScriptReaderImpl();
		SqlRunner runner = new SqlRunnerImpl();
		runner.run(scriptReader.createQueries("createTables.sql"));
		runner.run(scriptReader.createQueries("fillData.sql"));
		runner.run(scriptReader.createQueries("testQuery.sql"));
		runner.run(scriptReader.createQueries("drop.sql"));
	}
}
