CREATE SEQUENCE  "USERS_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1; 
CREATE SEQUENCE  "POSTS_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;

create table Users ("id" number(20,0) not null primary key, "name" NVARCHAR2(30) not null, "surname" NVARCHAR2(30) not null, "birthdate" date not null);
create table Friendships (userid1 number(20,0) not null, userid2 number(20,0) not null, "fr_time" timestamp(6) not null, PRIMARY KEY (userid1, userid2), CONSTRAINT FK_from_user1_to_user2 FOREIGN KEY (userid1) REFERENCES Users ("id"), CONSTRAINT FK_from_user2_to_user1 FOREIGN KEY (userid2) REFERENCES Users ("id"));
create table Posts ("id" number(20,0) not null primary key, userid number(20,0) not null, "text" NVARCHAR2(100) not null, "post_time" timestamp(6) not null, CONSTRAINT fk_user FOREIGN KEY (userid) REFERENCES users("id"));
create table Likes (postid number(20,0) not null, userid number(20,0) not null, "like_time" timestamp(6) not null, PRIMARY KEY (postid, userid), CONSTRAINT fk_usr FOREIGN KEY (userid) REFERENCES users("id"), CONSTRAINT fk_post FOREIGN KEY (postid) REFERENCES posts("id"));
  
    


      